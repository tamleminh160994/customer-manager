import Vue from "vue";
import Vuex from "vuex";
import * as customersData from "./modules/customersData.js";
import * as showSnackbar from "./modules/showSnackbar.js";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {},
    modules: {
        customersData,
        showSnackbar,
    },
    mutations: {},
    actions: {},
});