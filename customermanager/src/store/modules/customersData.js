import axios from "axios";

export const state = {
    customers: [],
    totalCustomers: null,
    customerById: {},
    customerByIdFullName: "",
    newCustomers: [],
};
export const mutations = {
    LOAD_CUSTOMERSDATA(state, { totalCustomers, customers }) {
        state.customers = customers;
        state.totalCustomers = totalCustomers;
    },
    ADD_NEWCUSTOMER(state, newCustomer) {
        state.customers.push(newCustomer);
    },
    GET_CUSTOMERBYID(state, id) {
        state.customerById = state.customers.find((customer) => customer.id === id);
        state.customerByIdFullName =
            state.customerById.firstName + " " + state.customerById.lastName;
    },
    DELETE_CUSTOMER(state, id) {
        state.customers = state.customers.filter((customer) => customer.id === !id);
    },
    EDIT_SELECTEDCUSTOMER(state, { id, newCustomer }) {
        var updateCustomer = state.customers.find((customer) => customer.id === id);
        Object.assign(updateCustomer, newCustomer);
    },
    FILTERCUSTOMERBYFIRSTNAME(state, filterFirstNameLowerCase) {
        state.customers = state.customers.filter((customer) => {
            return customer.firstName
                .toLowerCase()
                .startsWith(filterFirstNameLowerCase);
        });
    },
};
export const actions = {
    loadCustomersData({ commit }, { perPage, page }) {
        axios
            .get(
                "http://localhost:3000/customers?_limit=" + perPage + "&_page=" + page
            )
            .then((response) => {
                var totalCustomers = response.headers["x-total-count"];
                const customers = response.data;
                commit("LOAD_CUSTOMERSDATA", { totalCustomers, customers });
            });
    },
    addNewCustomer({ commit }, newCustomer) {
        axios
            .post("http://localhost:3000/customers", newCustomer)
            .then((response) => {
                commit("ADD_NEWCUSTOMER", response.data);
            });
    },
    getCustomerById({ commit }, id) {
        commit("GET_CUSTOMERBYID", id);
    },
    deleteCustomer({ commit }, id) {
        axios.delete(`http://localhost:3000/customers/${id}`).then(() => {
            commit("DELETE_CUSTOMER", id);
        });
    },
    editSeletedCustomer({ commit }, { id, newEditedCustomer }) {
        axios
            .put(`http://localhost:3000/customers/${id}`, newEditedCustomer)
            .then((response) => {
                var newCustomer = response.data;
                commit("EDIT_SELECTEDCUSTOMER", { id, newCustomer });
            });
    },
    filterCustomer({ commit }, filterFirstNameLowerCase) {
        commit("FILTERCUSTOMERBYFIRSTNAME", filterFirstNameLowerCase);
    },
};
export const getters = {};