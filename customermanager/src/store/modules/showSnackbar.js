export const state = {
    snackbarDisplay: false,
    snackbarTitle: "",
};
export const mutations = {
    SHOW_SNACKBARWHENCUSTOMERADDED(state) {
        state.snackbarDisplay = true;
        state.snackbarTitle = "Customer has been added!";
    },
    SHOW_SNACKBARWHENDELETED(state) {
        state.snackbarDisplay = true;
        state.snackbarTitle = "Customer has been deleted!";
    },
    SHOW_SNACKBARWHENEDITED(state) {
        state.snackbarDisplay = true;
        state.snackbarTitle = "Customer has been updated!";
    },
};
export const actions = {
    showSnackbarWhenCustomerAdded({ commit }) {
        commit("SHOW_SNACKBARWHENCUSTOMERADDED");
    },
    showSnackbarWhenDelete({ commit }) {
        commit("SHOW_SNACKBARWHENDELETED");
    },
    showSnackbarwhenEdited({ commit }) {
        commit("SHOW_SNACKBARWHENEDITED");
    },
};
export const getters = {};